#!/usr/bin/env python
desc="""Dumps phylomeDB trees to file."""
epilog="""Author:
l.p.pryszcz@gmail.com

Barcelona, 22/01/2013
"""

import argparse, os, sys
import gzip 
from ete2     import PhylomeDB3Connector
from datetime import datetime

'''
import MySQLdb
def _getConnection():
    """Return phylomeDB connection.
    """
    cnx = MySQLdb.connect(host="cgenomics.crg.es", user="phyReader", passwd="phyd10.-Reader", db='phylomedb', port=4040)
    return cnx

def get_phylomes(cur, public=False):
    """Return dictionary of phylomes.

    Returns only public phylomes if public=True"""
    
    cmd = "select phylome_id, name, description from phylome"
    if public:
        cmd += " where public=1"
    cur.execute(cmd)
    phylomes = {phyid: {"name": name, "description": description} for phyid, name, description in cur.fetchall()}  
    return phylomes
'''

def _getConnection():
    """Return phylomeDB connection.
    """
    p = PhylomeDB3Connector(host="cgenomics.crg.es",user="phyReader",passwd="phyd10.-Reader",db='phylomedb',port=4040)
    p._trees = "tree"
    p._phylomes = "phylome"
    p._algs = "alignment"
    p._phy_content = 'phylome_content'
    return p

def process_phylomes(outdir, replace, minid, maxid, verbose):
    """
    """
    #get output dir
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
    #connect
    if verbose: 
        sys.stderr.write("Downloading phylomes...\n")     
    p = _getConnection()
    phylomes = p.get_phylomes()
    i = 0
    for phyid in sorted(phylomes.keys()):
        if phyid<minid or phyid>maxid:
            continue
        i += 1
        outfn = "phylome_%5i.trees.gz" % phyid
        outfn = outfn.replace(" ","0")
        outfp = os.path.join(outdir,outfn)
        if not replace and os.path.isfile(outfp):
            continue
        info  = phylomes[phyid]
        if verbose: 
            sys.stderr.write( "[%s]  %s / %s [phylome_%s] %s\n" % ( datetime.ctime(datetime.now()),i,len(phylomes),phyid,info['name'] ) )
        #get MySQL cursor
        cnx = p._SQLconnection
        cur = cnx.cursor()
        l = cur.execute("SELECT protid,method,lk,newick FROM tree WHERE phylome_id=%s", (phyid, ))
        if not l:
            continue
        #open out file
        out = gzip.open(outfp,"w")
        for protid,method,lk,newick in cur.fetchall():
            out.write("%s\t%s\t%s\t%s\n"%(protid,method,lk,newick))
        out.close()
        
def main():
    usage   = "%(prog)s [options] -v" 
    parser  = argparse.ArgumentParser( usage=usage,description=desc,epilog=epilog )
  
    parser.add_argument("-v", dest="verbose", default=False, action="store_true", help="verbose")    
    parser.add_argument('--version', action='version', version='1.0')
    parser.add_argument("-r", dest="replace", default=False, action="store_true",
                        help="replace existing files [%(default)s]")    
    parser.add_argument("-o", dest="outdir",  default="trees", 
                        help="output dir             [%(default)s]")
    parser.add_argument("--min", dest="minid", default=1, type=int,
                        help="phylome_id to start from  [%(default)s]")
    parser.add_argument("--max", dest="maxid", default=1000, type=int,
                        help="phylome_id to start stop  [%(default)s]")
        
    o = parser.parse_args()
    if o.verbose:
        sys.stderr.write( "Options: %s\n" % str(o) )
        
    #download trees from phylomes
    process_phylomes(o.outdir, o.replace, o.minid, o.maxid, o.verbose)

if __name__=='__main__': 
    t0 = datetime.now()
    main()
    dt = datetime.now()-t0
    sys.stderr.write( "#Time elapsed: %s\n" % dt )
