#!/usr/bin/env python
desc="""Process all homologies and store predictions in MySQLdb.
NOTE: Get metahomologs during data loading, so no unnecessary memory use!
"""
epilog="""Author:
l.p.pryszcz@gmail.com

Barcelona, 17/11/2012
"""

import argparse, os, sys
import gzip, pickle, resource
import numpy as np
#from MyGraph  import HomologsGraph
from datetime import datetime

def load_ext2meta( fpath,taxids,verbose ):
    """Return ext2meta dict for given all taxid.
    """
    if verbose:
        sys.stderr.write( "Loading id2ext...\n" )
    if fpath.endswith(".gz"):
        handle = gzip.open(fpath)
    else:
        handle = open(fpath)
    #parse ext2meta file
    id2ext = {}
    for l in handle:
        protid,taxid,prot_name,gene_name,comments = l.split('\t')[:5]
        taxid = int(taxid)
        if taxids and taxid not in taxids:
            continue
        if taxid not in id2ext:
            id2ext[taxid] = {}
        id2ext[taxid][protid] = prot_name
            
    if verbose:
        sys.stderr.write( " id2ext for %s taxa loaded [memory %s MB]     \n" % ( len(id2ext),resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1000 ) )
            
    return id2ext

def add_homologs(homologs,v1,v2,ee):
    """Add relationship to dictionary"""
    #define pair
    if v1 < v2:
        pair = "%s-%s" % (v1,v2)
    else:
        pair = "%s-%s" % (v2,v1)
    #store score
    if pair not in homologs:
        homologs[pair] = int('0b1%s'%ee,2)
    else:
        homologs[pair] = int( bin(homologs[pair]) + str(ee),2 )
    return homologs
    
def taxid2metahomologs(taxid1,fpath,homologs,taxids,id2ext,verbose):
    """Add homologies to graph
    """
    i = k = 0
    for line in gzip.open(fpath):
        i += 1
        line = line.strip()
        taxid2, prot1, prot2, ee = line.split('\t')[:4]
        taxid2 = int(taxid2)
        #skip if taxa not requested
        if taxids and taxid2 not in taxids:
            continue
        #convert taxid
        if id2ext:
            try:
                ext1 = id2ext[taxid1][prot1]
                ext2 = id2ext[taxid2][prot2]
            except Exception as e:
                continue
        else:
            ext1,ext2 = prot1,prot2
        #store homologs
        #homologs.add_homologs(taxid1,ext1,taxid2,ext2,ee)
        homologs = add_homologs(homologs,ext1,ext2,ee)
        k += 1
    if i and verbose:
        sys.stdout.write("  %s\tcorrect pairs: %8i [%6.2f%s]\n" % (fpath,k,k*100.0/i,'%') )
    return homologs,k

def get_homologs(fpaths,taxids,id2extfp,verbose):
    """Load homology predictions and map ids into unified metaPhOrs ids.
    """
    id2ext = {}
    if id2extfp:
        id2ext = load_ext2meta(id2extfp,taxids,verbose)
    if verbose:
        sys.stdout.write("Loading homologies...\n")
    ii = 0
    homologs = {} #HomologsGraph()
    for fpath in sorted(fpaths):
        #get metahomologs
        fn     = os.path.basename(fpath)
        taxid1 = int(fn.split(".")[0])
        if taxids and taxid1 not in taxids:
            continue
        homologs,ih=taxid2metahomologs(taxid1,fpath,homologs,taxids,id2ext,verbose)
        ii += ih
    if verbose:
        sys.stdout.write( " %s homologies loaded [memory %s MB]\n" % ( len(homologs),resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1000 ) )
    return homologs

def get_orthologs(homologs,CSth=0.5,out=sys.stdout):
    """Report orthologs to output stream.
    By default to stdout."""
    sys.stderr.write( "Reporting orthologs to %s\n" % out.name )
    io = 0
    ih = len(homologs)
    for pair,iscore in homologs.items():
        v1,v2 = pair.split('-')
        #unload scores
        scores = [ int(x) for x in bin(iscore)[3:] ]
        #homologies store 0 for orthologs and 1 for paralogs
        if np.mean(scores) > CSth:
            continue
        #store homologs
        io += 1
        out.write( "%s\t%s\n" % (v1,v2) )

    sys.stderr.write( " %s homologs parsed. %s orthologs having CS >= %s reported.\n" % (ih,io,CSth) )
   
def main():
    usage   = "%(prog)s [options] -v -t taxid" 
    parser  = argparse.ArgumentParser( usage=usage,description=desc,epilog=epilog )
  
    parser.add_argument("-v", dest="verbose",   default=False, action="store_true", help="verbose")    
    parser.add_argument('--version', action='version', version='1.0')   
    parser.add_argument("-i", dest="files",     nargs="+", 
                        help="homologies files  [mandatory]")
    parser.add_argument("-t", dest="taxids",    default="", 
                        help="file with taxa    [All]" )
    parser.add_argument("-c", dest="CSth",      default=0.5, type=float,
                        help="CS cut-off        [%(default)s]" )
    parser.add_argument("-o", dest="out",       default="out", #type=argparse.FileType('w'),
                        help="output base name  [%(default)s]")
    parser.add_argument("-e", dest="id2extfp",  default='',
                        help="id2extfp file     [%(default)s]")
                          
    o = parser.parse_args()
    if o.verbose:
        sys.stderr.write( "Options: %s\n" % str(o) )

    #get taxids
    taxids = set()
    if o.taxids:
        taxids = set([int(l) for l in open(o.taxids)])
        
    #get homologs
    homologs = get_homologs( o.files,taxids,o.id2extfp,o.verbose )
    
    #store homologs
    picklefn = "%s.pickle" % o.out
    if o.verbose:
        sys.stderr.write("Dumping homologs to %s\n" % picklefn )
    outpickle = open(picklefn,"w")
    pickle.dump(homologs,outpickle,-1)
    outpickle.close()
    
    #store orthologs 
    out = gzip.open( "%s.orthologs.gz" % o.out,"w")
    #homologs.get_orthologs(o.CSth,out)
    get_orthologs(homologs,o.CSth,out)
    out.close()
	
if __name__=='__main__': 
    t0  = datetime.now()
    main()
    dt  = datetime.now()-t0
    sys.stdout.write( "#Time elapsed: %s\n" % dt )
