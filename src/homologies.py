#!/usr/bin/env python
"""Store functions to report homologies from ete2 tree object into tar archive.
"""

import os, sys
import gzip, gc
import resource
from datetime import datetime
import cStringIO, tarfile, time, zlib

def _get_taxid(node):
    """Return taxid from node.
    """
    code = node.taxid 
    return code

def _get_evolevent(childs):
    """Return 1 if any species overlap between
    any of childs. If no overlap, return 0.
    """
    #compare all childs
    for i in range(len(childs)-1):
        #get species
        spe1 = childs[i].get_species()
        for j in range(i+1, len(childs)):
            #get species
            spe2 = childs[j].get_species()
            #if species overlap
            if spe1.intersection(spe2):
                #duplication
                return 1
    #speciation
    return 0
    
def childs2files(subdir, homologies, childs1, childs2, ee, lk=None):
    """Add data to subdir/taxid1 gzipped file.
    taxid1 < taxid2
    """
    for l1 in childs1:
        t1data = (l1.taxid,l1.name)
        for l2 in childs2:
            t2data = (l2.taxid,l2.name)
            #make sure taxid1 < taxid2
            #if the same taxa, make sure extid1 < extid2
            (t1,e1), (t2,e2) = sorted([t1data, t2data])            
            #add data dict
            if t1 not in homologies:
                fpath = os.path.join(subdir,"%s.gz"%t1)
                homologies[t1] = gzip.open(fpath, "w")
            #add line to file
            homologies[t1].write("%s\t%s\t%s\t%s\n"%(t2, e1, e2, ee))
    return homologies

def tree2files(t, files, subdir):
    """Store all homologies from given tree into files
  
    Iterate through tree nodes:
     preorder    first parent and then children
     postorder   first children and the parent
     levelorder  nodes are visited in order from root to leaves
    """
    #traverse tree and store
    for node in t.traverse("preorder"):
        #skip predictions from root
        if node.is_root() or node.is_leaf():
            continue
        ###deal with multifurcations
        #get childs
        childs  = node.get_children()
        #get evolevent
        ee = _get_evolevent(childs)
        #get homologies
        for i in range(len(childs)-1):
            for j in range(i+1,len(childs)):                    
                #store relationships
                files = childs2files(subdir, files, childs[i], childs[j], ee)
    return files
    
def tree2files_c2s(t, files, subdir, ni=30):
    """Return all homologies from given tree
  
    Iterate through tree nodes:
     preorder    first parent and then children
     postorder   first children and the parent
     levelorder  nodes are visited in order from root to leaves
    """
    seedid = t.seedid
    if len(t.get_leaf_names())<ni:
        sclose = set(t.get_leaf_names())
    else:
        sclose = _get_close_to_seed(t,seedid,ni)
    #traverse tree and store
    for node in t.traverse("preorder"):
        #skip leaves
        if node.is_leaf():
            continue
        #skip nodes without sclose entries
        if not sclose.intersection(set(node.get_leaf_names())):
            continue
        ###deal with multifurcations
        #get childs
        childs  = node.get_children()
        #get evolevent
        ee = _get_evolevent(childs)
        #get homologies
        for i in range(len(childs)-1):
            for j in range(i+1,len(childs)):
                #skip pair where no sclose
                if childs[i] not in sclose and childs[j] in sclose:
                    continue
                #store relationships
                files = childs2files(subdir, files, childs[i], childs[j], ee)
    return files    
    
def trees2homologies_files(tree_parser, dbid, dbpath, treesFile, extid2treeidFile, verbose, iclose=0, threads=0):
    """Store homologies to gzipped file."""
    files = {}
    os.makedirs(dbpath)
    #iterate trees
    for trees, t in enumerate(tree_parser, 1):
        #if trees > 202: break
        
        #set species naming function
        t.set_species_naming_function(_get_taxid)
        
        #store newick if requested
        if treesFile:
            treesFile.write("%s\t%s\t%s\n" % (dbid, t.treeid, t.write()))

        #store extid2treeid
        for l in t.get_leaves(): 
            extid2treeidFile.write("%s\t%i\t%s\n" % (l.name, dbid, t.treeid))

        #store homologies in dict 
        if iclose:
            files = tree2files_c2s(t, files, dbpath, iclose)
        else:
            files = tree2files(t, files, dbpath)
        #write progress  
        info = ' %5i\tfiles: %s\tmemory: %s KB' % (trees, len(files), resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)
        if verbose:
            sys.stderr.write(info+"      \r")
            
    #print final info
    sys.stderr.write(info+"\n")
    
    #close extid2treeid file
    extid2treeidFile.close()
    
    #close if treesfile 
    if treesFile:
        treesFile.close()
        
    #if no trees processed        
    if not trees:
        #remove tmpdir
        os.system("rm -r %s" % dbpath)
        #remove extid2treeid file
        os.unlink(extid2treeidFile.name)
        #remove if treesfile
        if treesFile:
            os.unlink(treesFile.name)

###TAR COMPATIBILITY FUNCTIONS
def childs2dictstr(homologies, childs1, childs2, ee, lk=None):
    """Add data to subdir.db3 db in taxid1-taxid2 table.
    taxid1 < taxid2
    """
    for l1 in childs1:
        t1data = (l1.taxid,l1.name)
        for l2 in childs2:
            t2data = (l2.taxid,l2.name)
            #make sure taxid1 < taxid2
            #if the same taxa, make sure extid1 < extid2
            (t1,e1), (t2,e2) = sorted([t1data, t2data])
            #add pair of taxa to homologies
            tname = "%s-%s"%(t1,t2)
            if tname not in homologies:
                homologies[tname] = [] 
            #store in db
            homologies[tname].append("\t".join((e1, e2, str(ee))))
    return homologies

def tree2homologies(t, homologies):
    """Return all homologies from given tree
  
    Iterate through tree nodes:
     preorder    first parent and then children
     postorder   first children and the parent
     levelorder  nodes are visited in order from root to leaves
    """
    #traverse tree and store
    for node in t.traverse("preorder"):
        #skip predictions from root
        if node.is_leaf(): 
            continue
        ###deal with multifurcations
        #get childs
        childs  = node.get_children()
        #get evolevent
        ee = _get_evolevent(childs)
        #get homologies
        for i in range(len(childs)-1):
            for j in range(i+1,len(childs)):                    
                #store relationships
                homologies = childs2dictstr(homologies, childs[i], childs[j], ee)
    return homologies

def _get_close_to_seed(t, seedid, ni):
    """Return node with at max n closest to seed ids."""
    nok = t
    for n in t.traverse('postorder'):
        #skip nodes without seed
        if seedid not in n.get_leaf_names():
            continue
        #return node with seed <= ni leaves
        if len(n.get_leaf_names()) > ni:
            return set(nok.get_leaf_names())
        else:
            #store ok node
            nok = n
    return set(t.get_leaf_names())
    
def tree2homologies_c2s(t, homologies, ni=30):
    """Return all homologies from given tree
  
    Iterate through tree nodes:
     preorder    first parent and then children
     postorder   first children and the parent
     levelorder  nodes are visited in order from root to leaves
    """
    seedid = t.seedid
    if len(t.get_leaf_names())<ni:
        sclose = set(t.get_leaf_names())
    else:
        sclose = _get_close_to_seed(t,seedid,ni)
    #traverse tree and store
    for node in t.traverse("preorder"):
        #skip leaves
        if node.is_leaf():
            continue
        #skip nodes without sclose entries
        if not sclose.intersection(set(node.get_leaf_names())):
            continue
        ###deal with multifurcations
        #get childs
        childs  = node.get_children()
        #get evolevent
        ee = _get_evolevent(childs)
        #get homologies
        for i in range(len(childs)-1):
            for j in range(i+1,len(childs)):
                #skip pair where no sclose
                if childs[i] not in sclose and childs[j] in sclose:
                    continue
                #store relationships
                homologies = childs2dictstr(homologies, childs[i], childs[j], ee)
    return homologies    

def homologies2tar(homologies, dbpath):
    """Store homologies into gz.tar file"""
    #open tarfile with
    tar = tarfile.open(dbpath+".tar", "w")
    for i, (tpair, tdata) in enumerate(homologies.iteritems(), 1):
        #gz compress
        tdatagz = zlib.compress("\n".join(tdata))
        #get tarinfo
        ti = tarfile.TarInfo(tpair)
        ti.size  = len(tdatagz)
        ti.mtime = time.time()
        #add to tar
        tar.addfile(ti, cStringIO.StringIO(tdatagz))
    
def trees2homologies(tree_parser, dbid, dbpath, treesFile, extid2treeidFile, \
                     verbose, iclose=0, threads=0):
    """Store homologies to gzipped file."""
    homologies = {}
    info = trees = 0
    #iterate trees
    for trees, t in enumerate(tree_parser, 1):
        #if trees>1000: break
        #set species naming function
        t.set_species_naming_function(_get_taxid)
        #store newick if requested
        if treesFile:
            treesFile.write("%s\t%s\t%s\n" % (dbid, t.treeid, t.write()))
        #store extid2treeid
        for l in t.get_leaves(): 
            extid2treeidFile.write("%s\t%i\t%s\n" % (l.name, dbid, t.treeid))
        #disable garbage collection - python 2.6 patch
        if "2.6" < sys.version < "2.7.0":
            gc.disable()
        #store homologies in dict 
        if iclose:
            homologies = tree2homologies_c2s(t, homologies, iclose)
        else:
            homologies = tree2homologies(t, homologies)
        gc.enable()
        #write progress  
        info = ' %5i  taxa pairs: %s  memory: %s MB' % (trees, len(homologies), resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024)
        if verbose:
            sys.stderr.write(info+"      \r")
    #print final info
    if info:
        sys.stderr.write(info+"\n")
        #store homologies in tar file
        homologies2tar(homologies, dbpath)
    #close extid2treeid file
    extid2treeidFile.close()
    #close if treesfile 
    if treesFile:
        treesFile.close()
    #if no trees processed: remove extid2treeid file and remove if treesfile
    if not trees:
        os.unlink(extid2treeidFile.name)
        if treesFile:
            os.unlink(treesFile.name)

###SQLITE3 COMPATIBILITY FUNCTIONS
def childs2dict(homologies, childs1, childs2, ee, lk=None):
    """Add data to subdir.db3 db in taxid1-taxid2 table.
    taxid1 < taxid2
    """
    for l1 in childs1:
        t1data = (l1.taxid,l1.name)
        for l2 in childs2:
            t2data = (l2.taxid,l2.name)
            #make sure taxid1 < taxid2
            #if the same taxa, make sure extid1 < extid2
            (t1,e1), (t2,e2) = sorted([t1data, t2data])
    
            #add pair of taxa to homologies
            tname = "%s-%s"%(t1,t2)
            if tname not in homologies:
                homologies[tname] = []
            #store in db
            homologies[tname].append((e1, e2, ee))
    return homologies    
    
def tree2homologies_sqlite3(t, cur, tables):
    """Return all homologies from given tree
  
    Iterate through tree nodes:
     preorder    first parent and then children
     postorder   first children and the parent
     levelorder  nodes are visited in order from root to leaves
    """
    #traverse tree and store
    homologies = {}
    for node in t.traverse("preorder"):
        #skip predictions from root
        if node.is_root() or node.is_leaf():
            continue
        ###deal with multifurcations
        #get childs
        childs  = node.get_children()
        #get evolevent
        ee = _get_evolevent(childs)
        #get homologies
        for i in range(len(childs)-1):
            for j in range(i+1,len(childs)):                    
                #store relationships
                homologies = childs2dict(homologies, childs[i], childs[j], ee)

    #store in sqlite3
    for tname, ees in homologies.iteritems():
        #create new table for each pair of species
        if tname not in tables:
            tables.add(tname)
            cur.execute("create table `%s` (p1 TEXT, p2 TEXT, ee INT)"%tname)
        #store
        cur.executemany("insert into `"+tname+"` (p1, p2, ee) VALUES (?, ?, ?)", ees)
    return cur, tables

def sqlite3_connect(dbname):
    """Return sqlite connection and tables"""
    import sqlite3
    cnx = sqlite3.connect(dbname)
    cur = cnx.cursor()
    #get tables
    cur.execute("select name from sqlite_master where type = 'table';")
    tables = set(tname for tname, in cur.fetchall())
    return cur, tables
    
def trees2homologies_sqlite3(tree_parser, dbid, dbpath, treesFile, extid2treeidFile, verbose):
    """Store homologies to sqlite3 db."""
    #connect and get tables
    cur, tables = sqlite3_connect(dbpath)
    #iterate trees
    for trees, t in enumerate(tree_parser, 1):
        #set species naming function
        t.set_species_naming_function(_get_taxid)
        
        #store newick if requested
        if treesFile:
            treesFile.write("%s\t%s\t%s\n" % (dbid, t.treeid, t.write()))

        #store extid2treeid
        for l in t.get_leaves(): 
            extid2treeidFile.write("%s\t%i\t%s\n" % (l.name, dbid, t.treeid))

        #store homologies in dict
        cur, tables = tree2homologies_sqlite3(t, cur, tables)
        #write progress  
        info = ' %5i\tfiles: %s\tmemory: %s KB' % (trees, len(homologies), resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)
        if verbose:
            sys.stderr.write(info+"      \r")
            
    #print final info
    sys.stderr.write(info+"\n")
    
    #close extid2treeid file
    extid2treeidFile.close()
    
    #close if treesfile 
    if treesFile:
        treesFile.close()
        
    #if no trees processed        
    if not trees:
        #remove tmpdir
        os.system("rm %s" % dbpath)
        #remove extid2treeid file
        os.unlink(extid2treeidFile.name)
        #remove if treesfile
        if treesFile:
            os.unlink(treesFile.name)
    

            