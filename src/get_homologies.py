#!/usr/bin/env python
desc="""Parse trees from phylomeDB and store homologies into files.
If directory for given phylome exists, skip.

Note, predictions from root are not skipped from now on.
"""
epilog="""Author:
l.p.pryszcz@gmail.com

Barcelona, 24/01/2013
"""

import argparse, os, sys, time
import gzip, resource 
from ete2     import PhyloTree, PhylomeDB3Connector
from datetime import datetime
from phylome  import get_age_balanced_outgroup
from rooted_phylomes_all  import ROOTED_PHYLOMES
#sys.path.append("/isis/tg/lpryszcz/cluster/metaphors/src")
from homologies import trees2homologies
#from homologies_multi import trees2homologies
#from homologies import trees2homologies_files as trees2homologies

def _getConnection(triesLimit=50, sleep=5):
    """Return phylomeDB connection.
    Try 50 times every 5 seconds before giving up.
    """
    p = tries = 0
    while not p:
        try:
            p = PhylomeDB3Connector(host="cgenomics.crg.es", user="phyReader", \
                                    passwd="phyd10.-Reader", db='phylomedb', port=4040)
        except Exception, e:
            sys.stderr.write("Warning: Cannot connect to phylomeDB: %s\n"%e)
            tries += 1
            if tries > triesLimit:
                sys.stderr.write("Error: Gave up connecting to phylomeDB after %s trials!\n"%triesLimit)
                sys.exit(1)
            time.sleep(sleep)
    #superuser
    p._trees       = "tree"
    p._phylomes    = "phylome"
    p._algs        = "alignment"
    p._phy_content = 'phylome_content'
    return p

def _get_spcode(name):
    """Return spcode from protein."""
    code = name.split('_')[-1]
    return code
    
def phylome_tree_iterator(handle, phyid, sp2taxid, seedsp, rootingDict,\
                          balanced_root):
    """Phylome trees parser.
    """
    #process seedids
    for line in handle:
        line = line.strip()
        if not line:
            continue
        protid,method,lk,nw = line.split('\t')
        #process methods/trees for given seed
        lk = float(lk)
        t  = PhyloTree(nw)
        #skip NJ trees
        if not lk:
            continue
        #get treeid
        seedid    = "Phy%s_%s" % (protid,seedsp)
        treeid    = 'Phy%s_%s_%s' % (protid,phyid,method)
        t.treeid  = treeid
        t.seedid  = "Phy%s" % protid
        #get seedNode
        try:
            seedNode = t.get_leaves_by_name(seedid)[0]
        except:
            sys.stderr.write("Err: Cannot get seedid (%s) leaf in: %s\n" % (seedid, treeid))
            continue

        #ROOT TREE with error avoinding loops - should be sorted out in the future!
        t.set_species_naming_function(_get_spcode)
        try:
            #use balanced rooting
            if balanced_root:
                outgroup = get_age_balanced_outgroup(t,rootingDict)
            #use farthest rooting
            else:
                outgroup = seedNode.get_farthest_oldest_leaf(rootingDict)
            #root
            t.set_outgroup(outgroup)
        except Exception as e:
            sys.stderr.write( "Err: Cannot root tree %s: %s\n" % (treeid, e))
            continue

        try:
            # Phy000ABCD_HUMAN -> Phy000ABCD
            for l in t.get_leaves(): 
                extid,sp  = l.name.split('_')
                taxid     = sp2taxid[sp]
                l.name    = extid
                l.taxid   = taxid
        except Exception as e:
            sys.stderr.write("Err: Cannot set tree info in %s: %s\n" % (treeid, e))
            continue
        yield t
           
def phylomedb2homologies(fpaths, dbname, dbid, balanced_root, iclose, threads, \
                         verbose):
    """Process only phylomes that are not already deposited.
    """
    for fpath in fpaths:
        #trees/phylome_00001.trees.gz > phylome_00001.trees.gz > 1
        fn    = os.path.basename(fpath)
        phyid = int(fn.split(".")[0].split("_")[1])
        #rootingDict
        if phyid not in ROOTED_PHYLOMES:
            sys.stderr.write("%s not in ROOTED_PHYLOMES!\n"%phyid)
            sys.exit(1)
        rootingDict = ROOTED_PHYLOMES[phyid]

        #check if db present
        #db/trees/phylome_00001.trees.gz > db/homologies.phylome.00001
        wdir   = os.path.dirname(os.path.dirname(fpath))
        sdir   = "homologies.%s.%5i"%(dbname, phyid)
        sdir   = sdir.replace(" ","0")
        dbpath = os.path.join(sdir)
        if os.path.isfile(dbpath+".tar"):
        #if os.path.isdir(dbpath):
            continue
        if verbose:
            sys.stderr.write("[%s] %s dbid:%s %s\n" % (datetime.ctime(datetime.now()), dbname, dbid, dbpath))
        #get phylome info    
        p        = _getConnection()
        phyinfo  = p.get_phylome_info(phyid)
        #get sp2taxid
        protinfo  = p.get_proteomes_in_phylome(phyid)
        seedprot  = protinfo['seed']
        seedsp    = seedprot.split(".")[0]
        proteomes = protinfo['proteomes']
        sp2taxid = {}
        for proteomeid, infoDict in proteomes.iteritems():
            sp  = proteomeid.split('.')[0]
            sp2taxid[sp] = infoDict['taxid']

        #open outfiles
        treesFile = None
        extidfn   = '%s_%5i.extid2treeid.tbl.gz' % (dbname, phyid)
        extidfn   = extidfn.replace(" ","0")
        extidfp   = os.path.join(wdir, extidfn)
        extid2treeidFile = gzip.open(extidfp, 'w')

        #get tree iterator object
        out = gzip.open(fpath)
        treesIter = phylome_tree_iterator(out, phyid, sp2taxid, seedsp, \
                                          rootingDict, balanced_root)
        trees2homologies(treesIter, dbid, dbpath, treesFile, extid2treeidFile, \
                         verbose, iclose, threads)
        out.close()
        
def main():
    usage   = "%(prog)s [options] -v" 
    parser  = argparse.ArgumentParser(usage=usage, description=desc, epilog=epilog)
  
    parser.add_argument("-v", dest="verbose", default=False, action="store_true", help="verbose")    
    parser.add_argument('--version', action='version', version='1.0')
    parser.add_argument("-i", dest="input",   nargs="+",
                        help="trees files")
    parser.add_argument("-d", dest="dbid",    default=8, type=int,
                        help="database identifier           [%(default)s]")
    parser.add_argument("-n", dest="dbname",  default="phylome", 
                        help="database name                 [%(default)s]")
    parser.add_argument("-b", dest="balanced_root", default=False, action="store_true", 
                        help="age balance rooting           [%(default)s]")   
    parser.add_argument("-c", dest="iclose",  default=30, type=int,
                        help="close2seed limit              [%(default)s]")   
    parser.add_argument("-t", "--threads",    default=8, type=int,
                        help="number of threads to use      [%(default)s]")  
    
    o = parser.parse_args()
    if o.verbose:
        sys.stderr.write( "Options: %s\n" % str(o) )
        
    #download trees from phylomes
    phylomedb2homologies(o.input, o.dbname, o.dbid, o.balanced_root, o.iclose, \
                         o.threads, o.verbose)

if __name__=='__main__': 
    t0 = datetime.now()
    try:
        main()
    except KeyboardInterrupt:
        sys.stderr.write("\nCtrl-C pressed!      \n")
    dt = datetime.now()-t0
    sys.stderr.write( "#Time elapsed: %s\n" % dt )
