#!/usr/bin/env python
# Generate rooted phylomes

# USAGE: ./get_rooters.py species.tree > rooted_phylomes.py

import sys
import ete2

fn = sys.argv[1]
t = ete2.Tree(fn)

sp2age = {}
for l in t.get_leaves():
  seedsp = l.name
  sp2age[seedsp] = {seedsp: 1}
  for age, a in enumerate(l.get_ancestors(), 2):
    for sp in a.get_leaf_names():
      if sp not in sp2age[seedsp]:
        sp2age[seedsp][sp] = age

sys.stdout.write( 'ROOTED_PHYLOMES = ' + repr(sp2age) + '\n' )


