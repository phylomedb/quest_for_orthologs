#!/usr/bin/env python
"""Convert orthologs to ext ids
USAGE: zcat orthologs.gz | python orthologs2ext.py id2ext.txt.gz > orthologs.ext.gz
"""

import gzip, os, sys

def load_id2ext(fpath):
    """
    """
    if fpath.endswith(".gz"):
        handle = gzip.open(fpath)
    else:
        handle = open(fpath)
    #parse ext2meta file
    id2ext = {}
    for l in handle:
        protid,taxid,prot_name,gene_name,comments = l.split('\t')[:5]
        id2ext[protid] = prot_name
    return id2ext
        
sys.stderr.write("Loading ext2meta...\n")
id2ext = load_id2ext(sys.argv[1])
for l in sys.stdin:
    p1,p2 = l.split()
    try:
        e1=id2ext[p1]
        e2=id2ext[p2]
    except:
        continue
    print "%s\t%s" % (e1,e2)