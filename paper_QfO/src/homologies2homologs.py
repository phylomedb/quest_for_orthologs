#!/usr/bin/env python
desc="""Process all homologies and report homologs.
"""
epilog="""Author:
l.p.pryszcz@gmail.com

Barcelona, 25/09/2013
"""

import argparse, os, sys, tarfile
import gzip, resource, zlib
import numpy as np
from multiprocessing import Process, Manager, active_children, Queue
from datetime import datetime
from homologies2dict import load_ext2meta, add_homologs
    
def add_homologies(pair2signal, v1, v2, ee):
    """Add relationship to dictionary
    0 - speciation (orthologs)
    1 - duplication (paralogs)
    """
    #define pair
    pair = "%s-%s" % (v1, v2)
    #store score
    if pair not in pair2signal:
        pair2signal[pair] = int('0b1%s'%ee, 2)
    else:
        pair2signal[pair] = int(bin(pair2signal[pair]) + str(ee), 2)
    return pair2signal
   
def tpair2metahomologs(tpair, fpaths, id2ext, verbose):
    """Process homologies from tar for given pair of taxa and report
    homologous pairs with CS scores and trees.
    """
    pair2signal = {}
    #get taxa pair
    taxid1, taxid2 = tpair.split("-")
    #taxid1, taxid2 = int(taxid1), int(taxid2)
    #process tar files containing given pair of taxa
    for fpath in fpaths:
        i = k = 0
        #open tar, get tar file member and load zlib compressed lines
        tar    = tarfile.open(fpath, 'r')
        m      = tar.getmember(tpair)
        zlines = "".join(tar.extractfile(m).readlines())
        #process zdecompressed lines
        for i, line in enumerate(zlib.decompress(zlines).split('\n'), 1):
            if not line:
                continue
            #catch exceptions
            try:
                prot1, prot2, ee = line.split('\t')
            except:
                sys.stderr.write("Warning: Cannot unload line for %s from %s: %s\n"%(tpair, fpath, line))
            #convert protid to extid taxid
            if id2ext:
                try:
                    ext1 = id2ext[taxid1][prot1]
                    ext2 = id2ext[taxid2][prot2]
                except Exception as e:
                    continue
            else:
                ext1, ext2 = prot1, prot2
            #sort extids if same taxa and ext2 < ext1
            if taxid1 == taxid2 and ext2 < ext1:
                ext1, ext2 = ext2, ext1
            #store homologs
            pair2signal = add_homologies(pair2signal, ext1, ext2, ee)
            k += 1
        if i and verbose:
            sys.stdout.write("  %s\t%s\tcorrect: %8i [%6.2f%s]\n" % (tpair, fpath, k, k*100.0/i, '%'))
    #get CS per each prediction
    homologs = []
    for pair, intsignals in pair2signal.iteritems():
        ext1, ext2 = pair.split('-')
        #unload intsignals into ee
        signals   = [int(b) for b in bin(intsignals)[3:]]
        CS, trees = 1.0-np.mean(signals), len(signals)
        #append to homologs
        homologs.append("%s\t%s\t%s\t%s\t%.3f\t%s\n"%(taxid1, taxid2, ext1, ext2, CS, trees))
    return tpair, homologs

def worker(input, output, id2ext, verbose):
    for tpair, fpaths in iter(input.get, 'STOP'):
        result = tpair2metahomologs(tpair, fpaths, id2ext, verbose)
        output.put(result)

def get_taxa_pairs(fpaths, taxids, verbose):
    """Return all pairs of taxa"""
    if verbose:
        sys.stderr.write("Loading taxa pairs...\n")
    tpair2fpaths = {}
    for i, fpath in enumerate(fpaths, 1):
        if verbose:
            sys.stderr.write(" %s / %s  %s                       \r"%(i, len(fpaths), fpath))
        #process all taxa pairs
        for m in tarfile.open(fpath,'r'):
            tpair  = m.name
            t1, t2 = tpair.split('-')
            #t1, t2 = int(t1), int(t2)
            #store only if both pairs requested
            if taxids and t1 not in taxids or taxids and t2 not in taxids:
                continue
            if tpair not in tpair2fpaths:
                tpair2fpaths[tpair] = []
            tpair2fpaths[tpair].append(fpath)
    if verbose:
        sys.stderr.write(" %s taxa pairs loaded.           \n" % len(tpair2fpaths))
    return tpair2fpaths

def store_homologs(outfn, outQ, tpair2fpaths, verbose):
    """ """
    #open output handle
    if outfn.endswith('.gz'):
        out = gzip.open(outfn, "w")
    else:
        out =      open(outfn, "w")
    #write header
    out.write("#taxid1\ttaxid2\tprotid1\tprotid2\tCS\ttrees\n")
    #combine results of all processes
    if verbose:
        sys.stdout.write("Storing homologs to %s ...\n"%outfn)
    for i in xrange(len(tpair2fpaths)):
        tpair, homologs = outQ.get()
        out.write("".join(homologs))
        if verbose:
            sys.stdout.write("%s / %s  %s  homologs: %s  [%s MB] outQ: %s    \n" % (i+1, len(tpair2fpaths), tpair, len(homologs), resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1000, outQ.qsize()))
        
def homologies2homologs(outfn, fpaths, taxids, id2extfp, nprocs, verbose):
    """Load homology predictions and map ids into unified metaPhOrs ids.
    """
    #load id2ext
    id2ext = {}
    if id2extfp:
        id2ext = load_ext2meta(id2extfp, taxids, verbose)
    #load taxa pairs from tar files
    tpair2fpaths = get_taxa_pairs(fpaths, taxids, verbose)
    #define in & out queue
    inQ, outQ = Queue(), Queue()
    #populate inQ
    for tpair, fpaths in tpair2fpaths.iteritems():
        inQ.put((tpair, fpaths))
    #run multiprocesses & put stop to all childs
    sverbose = 0
    for i in range(nprocs):
        p = Process(target=worker, args=(inQ, outQ, id2ext, sverbose))
        p.start()
        inQ.put('STOP')
    #store homologs
    store_homologs(outfn, outQ, tpair2fpaths, verbose)
    
def main():
    usage   = "%(prog)s [options] -v -t taxid" 
    parser  = argparse.ArgumentParser(usage=usage, description=desc, epilog=epilog)
  
    parser.add_argument("-v", dest="verbose",   default=False, action="store_true", help="verbose")    
    parser.add_argument('--version', action='version', version='1.0')   
    parser.add_argument("-i", dest="files",     nargs="+", 
                        help="homologies files  [mandatory]")
    parser.add_argument("-t", dest="taxids",    default="", 
                        help="file with taxa    [All]" )
    parser.add_argument("-o", dest="out",       default="homologs.gz", 
                        help="output file name  [%(default)s]")
    parser.add_argument("-e", dest="id2extfp",  default='',
                        help="id2extfp file     [%(default)s]")
    parser.add_argument("-p", "--threads",      default=4, type=int,
                        help="number of threads [%(default)s]" )
                          
    o = parser.parse_args()
    if o.verbose:
        sys.stderr.write("Options: %s\n" % str(o))

    #get taxids
    taxids = set()
    if o.taxids:
        taxids = set([int(l) for l in open(o.taxids)])
    #run
    homologies2homologs(o.out, o.files, taxids, o.id2extfp, o.threads, o.verbose)
    
if __name__=='__main__': 
    t0  = datetime.now()
    try:
        main()
    except KeyboardInterrupt:
        sys.exit("\nCtrl-C pressed!      ")
    dt  = datetime.now()-t0
    sys.stdout.write("#Time elapsed: %s\n" % dt)
