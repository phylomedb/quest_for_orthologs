#!/usr/bin/env python
desc="""Process all homologies and store predictions in MySQLdb.
NOTE: Get metahomologs during data loading, so no unnecessary memory use!
"""
epilog="""Author:
l.p.pryszcz@gmail.com

Barcelona, 17/11/2012
"""

import argparse, os, sys
import gzip, pickle, resource
from MyGraph  import HomologsGraph
from datetime import datetime

def write_OrthoXML():
    """
<orthoXML xmlns="http://orthoXML.org/2011/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="0.3" origin="inparanoid" originVersion="7.0" xsi:schemaLocation="http://orthoXML.org/2011/ http://www.orthoxml.org/0.3/orthoxml.xsd">
<notes>
Example OrthoXML file. Stripped down version of a real InParanoid 7.0 file.
</notes>
<species name="Caenorhabditis elegans" NCBITaxId="6239">
<database name="WormBase" version="Caenorhabditis-elegans_WormBase_WS199_protein-all.fa" geneLink="http://www.wormbase.org/db/gene/gene?name=" protLink="http://www.wormbase.org/db/seq/protein?name=WP:">
<genes>
<gene id="1" geneId="WBGene00000962" protId="CE23997"/>
<gene id="5" geneId="WBGene00006801" protId="CE43332"/>
</genes>
</database>
</species>
<species name="Homo Sapiens" NCBITaxId="9606">
<database name="Ensembl" version="Homo_sapiens.NCBI36.52.pep.all.fa" geneLink="http://Dec2008.archive.ensembl.org/Homo_sapiens/geneview?gene=" protLink="http://Dec2008.archive.ensembl.org/Homo_sapiens/protview?peptide=">
<genes>
<gene id="2" geneId="ENSG00000197102" protId="ENSP00000348965"/>
<gene id="6" geneId="ENSG00000198626" protId="ENSP00000355533"/>
<gene id="7" protId="ENSP00000373884"/>
</genes>
</database>
</species>
<scores>
<scoreDef id="bit" desc="BLAST score in bits of seed orthologs"/>
<scoreDef id="inparalog" desc="Distance between edge seed ortholog"/>
<scoreDef id="bootstrap" desc="Reliability of seed orthologs"/>
</scores>
<groups>
<orthologGroup id="1">
<score id="bit" value="5093"/>
<property name="foo" value="bar"/>
<geneRef id="1">
<score id="inparalog" value="1"/>
<score id="bootstrap" value="1.00"/>
</geneRef>
<geneRef id="2">
<score id="inparalog" value="1"/>
<score id="bootstrap" value="1.00"/>
</geneRef>
</orthologGroup>
<orthologGroup id="3">
<score id="bit" value="3795"/>
<geneRef id="5">
<score id="inparalog" value="1"/>
<score id="bootstrap" value="1.00"/>
</geneRef>
<geneRef id="6">
<score id="inparalog" value="1"/>
<score id="bootstrap" value="1.00"/>
</geneRef>
<geneRef id="7">
<score id="inparalog" value="0.4781"/>
</geneRef>
</orthologGroup>
</groups>
</orthoXML>    
    """
    pass

def load_ext2meta( fpath,taxids,verbose ):
    """Return ext2meta dict for given all taxid.
    """
    if verbose:
        sys.stderr.write( "Loading id2ext...\n" )
    if fpath.endswith(".gz"):
        handle = gzip.open(fpath)
    else:
        handle = open(fpath)
    #parse ext2meta file
    id2ext = {}
    for l in handle:
        protid,taxid,prot_name,gene_name,comments = l.split('\t')[:5]
        taxid = int(taxid)
        if taxids and taxid not in taxids:
            continue
        if taxid not in id2ext:
            id2ext[taxid] = {}
        id2ext[taxid][protid] = prot_name
            
    if verbose:
        sys.stderr.write( " id2ext for %s taxa loaded [memory %s MB]     \n" % ( len(id2ext),resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1000 ) )
            
    return id2ext
    
def taxid2metahomologs( taxid1,fpath,homologs,taxids,id2ext,verbose ):
    """Add homologies to graph
    """
    i = k = 0
    for line in gzip.open(fpath):
        i += 1
        line = line.strip()
        taxid2, prot1, prot2, ee = line.split('\t')[:4]
        taxid2 = int(taxid2)
        #skip if taxa not requested
        if taxids and taxid2 not in taxids:
            continue
        #convert taxid
        ext1,ext2 = prot1,prot2
        if id2ext:
            try:
                ext1 = id2ext[taxid1][prot1]
                ext2 = id2ext[taxid2][prot2]
            except Exception as e:
                continue
        #store homologs
        homologs.add_homologs( taxid1,ext1,taxid2,ext2,ee )
        k += 1
    if i and verbose:
        sys.stdout.write("  %s\tcorrect pairs: %8i [%6.2f%s]\n" % (fpath,k,k*100.0/i,'%') )

def get_homologs( fpaths,taxids,id2extfp,verbose ):
    """Load homology predictions and map ids into unified metaPhOrs ids.
    """
    id2ext = {}
    if id2extfp:
        id2ext = load_ext2meta( id2extfp,taxids,verbose )
    if verbose:
        sys.stdout.write("Loading homologies...\n")
    homologs = HomologsGraph()
    for fpath in sorted(fpaths):
        #get metahomologs
        fn     = os.path.basename(fpath)
        taxid1 = int(fn.split(".")[0])
        if taxids and taxid1 not in taxids:
            continue
        taxid2metahomologs(taxid1,fpath,homologs,taxids,id2ext,verbose)
    
    if verbose:
        sys.stdout.write( " %s homologies for %s taxa loaded [memory %s MB]\n" % ( homologs.homologiesCount,len(homologs),resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1000 ) )
    return homologs
    
def main():
    usage   = "%(prog)s [options] -v -t taxid" 
    parser  = argparse.ArgumentParser( usage=usage,description=desc,epilog=epilog )
  
    parser.add_argument("-v", dest="verbose",   default=False, action="store_true", help="verbose")    
    parser.add_argument('--version', action='version', version='1.0')   
    parser.add_argument("-i", dest="files",     nargs="+", 
                        help="homologies files  [mandatory]")
    parser.add_argument("-t", dest="taxids",    default="", 
                        help="file with taxa    [All]" )
    parser.add_argument("-c", dest="CSth",      default=0.5, type=float,
                        help="CS cut-off        [%(default)s]" )
    parser.add_argument("-o", dest="out",       default="out", #type=argparse.FileType('w'),
                        help="output base name  [%(default)s]")
    parser.add_argument("-e", dest="id2extfp",  default='',
                        help="id2extfp file     [%(default)s]")
                          
    o = parser.parse_args()
    if o.verbose:
        sys.stderr.write( "Options: %s\n" % str(o) )

    #get taxids
    taxids = set()
    if o.taxids:
        taxids = set([int(l) for l in open(o.taxids)])
        
    #get homologs
    homologs = get_homologs( o.files,taxids,o.id2extfp,o.verbose )
    
    #store homologs
    picklefn = "%s.pickle" % o.out
    if o.verbose:
        sys.stderr.write("Dumping homologs to %s\n" % picklefn )
    outpickle = open( picklefn,"w" )
    pickle.dump(homologs,outpickle,-1)
    outpickle.close()

    #store orthologs 
    out = gzip.open( "%s.orthologs.gz" % o.out,"w")
    homologs.get_orthologs(o.CSth,out)
    out.close()
	
if __name__=='__main__': 
    t0  = datetime.now()
    main()
    dt  = datetime.now()-t0
    sys.stdout.write( "#Time elapsed: %s\n" % dt )
